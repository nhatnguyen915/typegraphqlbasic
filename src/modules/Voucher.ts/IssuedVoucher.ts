import { Resolver, Mutation, Arg } from "type-graphql";
import { Connection, getCustomRepository, Transaction, TransactionRepository } from "typeorm";
import { Voucher } from "../../models/Voucher";
import { EventRepository } from "../../repositories/EventRepository";
import { VoucherRepository } from "../../repositories/VoucherRepository";
import { CreateVoucherInput } from "../../types/CreateVoucherInput";
import { getManager } from "typeorm";
import { InjectConnection } from "typeorm-typedi-extensions";
import { ApolloError } from "apollo-server-express";
@Resolver((_type) => Voucher)
export class CreateVoucher {
    constructor(@InjectConnection() private connection: Connection) { }
    @Mutation((_type) => Voucher)
    public async createVoucher(
        @Arg("data") inputData: CreateVoucherInput
    ): Promise<Voucher> {
        let voucherRsp = new Voucher;

        await getManager().transaction(async transactionalEntityManager => {
            const eventRepository = transactionalEntityManager.getCustomRepository(EventRepository);
            const event = await eventRepository.findOne(inputData.eventId);
            console.log(event)
            if (!event)
                throw new ApolloError("Event not found")
            eventRepository.update({
                id: event.id,
            }, {
                quantity: event.quantity - 1,
            });
            const eventUpdated = await eventRepository.findOne(inputData.eventId);
            console.log(eventUpdated);
            if (eventUpdated.quantity < 0) {
                console.log();
                throw new ApolloError("Sold out")
            }
            const voucherRepository = transactionalEntityManager.getCustomRepository(VoucherRepository);
            const voucher = await voucherRepository.save({
                name: inputData.name,
                code: inputData.code,
                eventId: event.id
            });
            voucherRsp = voucher;

        });
        return voucherRsp;

    }
}