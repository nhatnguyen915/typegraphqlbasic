
import { Arg, Ctx, Mutation, Resolver } from 'type-graphql'
import { getCustomRepository } from 'typeorm';
import { UserRepository } from '../../repositories/UserRepository';
import { UserMutationResponse } from '../../types/UserMutaionReponse';
import bcrpt from 'bcrypt';
import { RegisterInput } from '../../types/RegisterInput';
import { User } from '../../models/User';
import { Context } from '../../types/Context';
import { LoginInput } from '../../types/LoginInput';
import { createToken, sendRefreshToken } from '../../utils/jwtService';

@Resolver((_type) => User)
export class Register {
    @Mutation(_return => UserMutationResponse)
    public async registerUser(
        @Arg("data") inputData: RegisterInput
    ): Promise<UserMutationResponse> {

        const userRepository = getCustomRepository(UserRepository);
        const existingUser = await userRepository.findOne(inputData.username)

        if (existingUser) {
            return {
                code: 400,
                success: false,
                message: 'Duplicated username'
            }
        }
        const salt = await bcrpt.genSalt(10);
        const hashPassword = await bcrpt.hash(inputData.password, salt)

        const category = userRepository.create({
            username: inputData.username,
            password: hashPassword,
        });

        const newUser = await userRepository.save(category);
        return {
            code: 200,
            success: true,
            message: 'User registration successful',
            user: newUser
        }
    }
}
export class Login {
    @Mutation(_return => UserMutationResponse)
    public async login(
        @Arg('data') inputData: LoginInput,
        @Ctx() { res }: Context
    ): Promise<UserMutationResponse> {
        const existingUser = await User.findOne({ username: inputData.username })

        if (!existingUser) {
            return {
                code: 400,
                success: false,
                message: 'User not found'
            }
        }
        console.log(existingUser.password)
        console.log(inputData.password)

        const isPasswordValid = await bcrpt.compare(inputData.password, existingUser.password)
        console.log(isPasswordValid)

        if (!isPasswordValid) {
            return {
                code: 400,
                success: false,
                message: 'Incorrect password'
            }
        }

        sendRefreshToken(res, existingUser)

        return {
            code: 200,
            success: true,
            message: 'Logged in successfully',
            user: existingUser,
            accessToken: createToken('accessToken', existingUser)
        }
    }
}

