
import { Arg, Ctx, Int, Mutation, Resolver, UseMiddleware } from "type-graphql";
import { getCustomRepository } from "typeorm";
import { authUser } from "../../middleware/authUser";
import { EventEditLog } from "../../models/EventEditLog";
import { EventRepository } from "../../repositories/EventRepository";
import { Context } from "../../types/Context";
import { EditableMutationResponse } from "../../types/EditableMutationReponse";
import { exists, setnx, get, incrby, ttl, buildEventkey } from "../../utils/redisService"
@Resolver((_type) => EventEditLog)
export class EditableMe {
    @Mutation(_return => EditableMutationResponse)
    @UseMiddleware(authUser)
    public async editTableMe(
        @Arg("eventId", () => Int) eventId: number, @Ctx() { user }: Context
    ): Promise<EditableMutationResponse> {
        const eventResponse = getCustomRepository(EventRepository);
        const event = await eventResponse.findOne(eventId);
        if (!event) {
            return {
                code: 400,
                success: false,
                message: "Event not found"
            }
        }
        // const keyStr: string = eventId.toString() + '-editing';
        const keyStr = buildEventkey(eventId);
        const keyName = await exists(keyStr);
        if (!keyName) {
            await setnx(keyStr, 0, 300) // set expire 300s
        }
        const count = await incrby(keyStr, 1);
        if (count > 1) {
            await incrby(keyStr, -1)
            if (event.userEditing == user.userId) {
                return {
                    code: 200,
                    success: true,
                    message: 'Successfully ',
                }

            }
            const timeExp = await ttl(keyStr);
            return {
                code: 409,
                success: false,
                message: 'Event is editing, try again after: ' + timeExp + 's',
            }
        }
        else {
            event.userEditing = user.userId;
            eventResponse.save(event)
            return {
                code: 200,
                success: true,
                message: 'Successfully ',
            }
        }


    }
}