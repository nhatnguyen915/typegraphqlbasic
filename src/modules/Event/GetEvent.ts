import { Resolver, Query, UseMiddleware } from "type-graphql";
import { InjectRepository } from "typeorm-typedi-extensions";
import { authUser } from "../../middleware/authUser";

import { Event } from "../../models/Event";
import { EventRepository } from "../../repositories/EventRepository";

@Resolver((_type) => Event)
export class GetEvent {
    constructor(
        @InjectRepository()
        private readonly eventRepository: EventRepository
    ) { }

    @Query((_type) => [Event])
    @UseMiddleware(authUser)
    public async getEvent(): Promise<Event[]> {
        const event = await this.eventRepository.find({
            relations: ["voucher"],
        }
        );

        return event;
    }
}