
import { Arg, Ctx, Int, Mutation, Resolver, UseMiddleware } from "type-graphql";
import { getCustomRepository } from "typeorm";
import { authUser } from "../../middleware/authUser";
import { EventEditLog } from "../../models/EventEditLog";
import { EventRepository } from "../../repositories/EventRepository";
import { Context } from "../../types/Context";
import { EditableMutationResponse } from "../../types/EditableMutationReponse";
import { exists, setnx, get, expire, buildEventkey } from "../../utils/redisService"
@Resolver((_type) => EventEditLog)
export class EditableMaintain {
    @Mutation(_return => EditableMutationResponse)
    @UseMiddleware(authUser)
    public async editableMaintain(
        @Arg("eventId", () => Int) eventId: number, @Ctx() { user }: Context
    ): Promise<EditableMutationResponse> {
        const eventResponse = getCustomRepository(EventRepository);
        const event = await eventResponse.findOne(eventId);
        if (!event) {
            return {
                code: 400,
                success: false,
                message: "Event not found"
            }
        }
        const keyStr = buildEventkey(eventId);
        const keyName = await exists(keyStr);
        if (!keyName) {
            return {
                code: 400,
                success: true,
                message: 'Can not relase event is not editing  ',
            }
        }
        if (event.userEditing != user.userId) {
            return {
                code: 200,
                success: true,
                message: 'This user can not relase event ',
            }
        }
        expire(keyStr, 300)// set expire 300s
        return {
            code: 200,
            success: true,
            message: 'Successfully ',
        }

    }
}