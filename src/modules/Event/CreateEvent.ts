import { Arg, Mutation, Resolver, UseMiddleware } from "type-graphql";
import { getCustomRepository } from "typeorm";
import { authUser } from "../../middleware/authUser";
import { Event } from "../../models/Event";
import { EventRepository } from "../../repositories/EventRepository";
import { CreateEventInput } from "../../types/CreateEventInput";

@Resolver((_type) => Event)
export class CreateEvent {
    @Mutation((_type) => Event)
    @UseMiddleware(authUser)
    public async createEvent(
        @Arg("data") inputData: CreateEventInput
    ): Promise<Event> {
        const eventRepository = getCustomRepository(EventRepository);
        const category = eventRepository.create({
            name: inputData.name,
            code: inputData.code,
            quantity: inputData.quantity
        });

        await eventRepository.save(category);
        return category;
    }
}