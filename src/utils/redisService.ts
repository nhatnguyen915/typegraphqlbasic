const redis = require("redis")
const client = redis.createClient();
(async () => {
    await client.connect();
})();

client.on('connect', () => console.log('::> Redis Client Connected'));
client.on('error', (err: String) => console.log('<:: Redis Client Error', err));

export const get = async (key: string) => {
    return new Promise((resolve, reject) => {
        try {
            const result = client.get(key);
            resolve(result);
        } catch (error) {
            reject(error);
        }
    })
}
export const exists = async (key: string) => {
    return new Promise((resolve, reject) => {
        try {
            const result = client.exists(key);
            resolve(result);
        } catch (error) {
            reject(error);
        }
    })
}
export const set = async (key: string, value: any, duration: number) => {
    return new Promise((resolve: (val: boolean) => void, reject: (val: string) => void) => {
        try {
            client.set(key, value);
            client.expire(key, duration);
            resolve(true);
        } catch (error) {
            return reject(error);
        }
    });
}
export const setnx = async (key: string, value: any, duration: number) => {
    return new Promise((resolve: (val: any) => void, reject: (val: string) => void) => {
        try {
            client.setNX(key, value);
            client.expire(key, duration);
            resolve(true);
        } catch (error) {
            return reject(error);
        }
    });
}
export const expire = async (key: string, duration: number) => {
    return new Promise((resolve: (val: boolean) => void, reject: (val: string) => void) => {
        try {
            client.expire(key, duration);
            resolve(true);
        } catch (error) {
            return reject(error);
        }
    });
}
export const incrby = async (key: string, value: any) => {
    return new Promise((resolve: (val: any) => void, reject: (val: string) => void) => {
        try {
            const result = client.incr(key, value);
            resolve(result);
        } catch (error) {
            return reject(error);
        }
    });
}
export const ttl = async (key: string) => {
    return new Promise((resolve: (val: any) => void, reject: (val: string) => void) => {
        try {
            const result = client.ttl(key);
            resolve(result);
        } catch (error) {
            return reject(error);
        }
    });
}
export const del = async (key: string) => {
    return new Promise((resolve: (val: boolean) => void, reject: (val: string) => void) => {
        try {
            client.del(key);
            resolve(true);
        } catch (error) {
            return reject(error);
        }
    });
}
export const buildEventkey = (eventId: number) => {
    return eventId.toString() + '-editing';
}