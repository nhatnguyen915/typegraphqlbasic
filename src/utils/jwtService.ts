import { Response } from 'express'
import { Secret, sign } from 'jsonwebtoken'
import { User } from '../models/User'

const token = "BbZJjyoXAdr8BUZuiKKARWimKfrSmQ6fv8kZ7OFfc"
const refreshToken = 'BbZJjyoXAdr8BUZuiKKARWimKfrSmQ6fv8kZ7OFfcPu'
const cookie = "jwt-auth-cookie-name"
export const createToken = (type: 'accessToken' | 'refreshToken', user: User) =>
    sign(
        {
            userId: user.id,
            ...(type === 'refreshToken' ? { tokenVersion: user.tokenVersion } : {})
        },
        type === 'accessToken'
            ? (token as Secret)
            : (refreshToken as Secret),
        {
            expiresIn: type === 'accessToken' ? '60m' : '60m'
        }
    )

export const sendRefreshToken = (res: Response, user: User) => {
    res.cookie(
        cookie as string,
        createToken('refreshToken', user),
        {
            httpOnly: true,
            secure: true,
            sameSite: 'lax',
            path: '/refresh_token'
        }
    )
}

