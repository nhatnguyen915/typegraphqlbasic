import { EntityRepository, Repository } from "typeorm";
import { Event } from "../models/Event";
import { EventEditLog } from "../models/EventEditLog";

@EntityRepository(EventEditLog)
export class EventEditLogRepository extends Repository<EventEditLog> { }