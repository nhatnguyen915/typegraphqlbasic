import { EntityRepository, Repository } from "typeorm";
import { Voucher } from "../models/Voucher";

@EntityRepository(Voucher)
export class VoucherRepository extends Repository<Voucher> { }