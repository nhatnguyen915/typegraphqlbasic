import {
    Column,
    CreateDateColumn,
    Entity,
    PrimaryGeneratedColumn,
    Unique,
    UpdateDateColumn,
    ManyToOne,
    JoinColumn,
} from "typeorm";
import { Field, ObjectType } from "type-graphql";

import { Event } from "./Event";
@ObjectType()
@Entity()
@Unique(["eventId"])
export class EventEditLog {
    @Field((_type) => Number)
    @PrimaryGeneratedColumn()
    public readonly id!: number;

    @Field()
    @Column()
    public userId!: number;

    @Field()
    @CreateDateColumn()
    public createdAt!: Date;

    @Field()
    @UpdateDateColumn()
    public updatedAt!: Date;

    @Field()
    @Column({ type: "number" })
    @Field((_type) => Event)
    public eventId!: number;
    @ManyToOne((_type) => Event, (event: Event) => event.voucher)
    @JoinColumn({ name: "eventId" })
    public event!: Event;
}