import {
    Column,
    CreateDateColumn,
    Entity,
    PrimaryGeneratedColumn,
    Unique,
    UpdateDateColumn,
    OneToMany,
} from "typeorm";
import { Field, ObjectType } from "type-graphql";

import { Voucher } from "./Voucher";
import { EventEditLog } from "./EventEditLog";
@ObjectType()
@Entity()
@Unique(["code"])
export class Event {
    @Field((_type) => Number)
    @PrimaryGeneratedColumn()
    public readonly id!: number;

    @Field()
    @Column({ type: "varchar" })
    public code!: string;

    @Field()
    @Column({ type: "varchar" })
    public name!: string;

    @Field((_type) => Number)
    @Column()
    public quantity!: number;

    @Field((_type) => Number)
    @Column()
    public userEditing: number = 0;

    @Field()
    @CreateDateColumn()
    public createdAt!: Date;

    @Field()
    @UpdateDateColumn()
    public updatedAt!: Date;

    @Field((_type) => [Voucher])
    @OneToMany((_type) => Voucher, (voucher: Voucher) => voucher.event)
    public voucher?: Voucher[];

    @Field((_type) => [EventEditLog])
    @OneToMany((_type) => EventEditLog, (eventEditLog: EventEditLog) => eventEditLog.event)
    public eventEditLog?: EventEditLog[];
}