import {
    Column,
    CreateDateColumn,
    Entity,
    PrimaryGeneratedColumn,
    Unique,
    UpdateDateColumn,
    ManyToOne,
    JoinColumn,
} from "typeorm";
import { Field, ObjectType } from "type-graphql";

import { Event } from "./Event";
@ObjectType()
@Entity()
@Unique(["code"])
export class Voucher {
    @Field((_type) => Number)
    @PrimaryGeneratedColumn()
    public readonly id!: number;

    @Field()
    @Column({ type: "varchar" })
    public code!: string;

    @Field()
    @Column({ type: "varchar" })
    public name!: string;

    @Field()
    @CreateDateColumn()
    public createdAt!: Date;

    @Field()
    @UpdateDateColumn()
    public updatedAt!: Date;

    @Field()
    @Column({ type: "number" })
    @Field((_type) => Event)
    public eventId!: number;
    @ManyToOne((_type) => Event, (event: Event) => event.voucher)
    @JoinColumn({ name: "eventId" })
    public event!: Event;
}