import { Field, ID, ObjectType } from 'type-graphql'
import { BaseEntity, Column, Entity, PrimaryGeneratedColumn, Unique } from 'typeorm'

@ObjectType()
@Entity()
@Unique(["username"])
export class User extends BaseEntity {
    @Field(_type => ID)
    @PrimaryGeneratedColumn()
    id!: number

    @Field()
    @Column({ unique: true })
    username!: string

    @Column()
    password!: string

    @Column({ default: 0 })
    tokenVersion: number
}