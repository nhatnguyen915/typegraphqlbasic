import { Field, InputType } from 'type-graphql'
import { User } from '../models/User'

@InputType()
export class RegisterInput implements Partial<User> {
	@Field()
	username: string

	@Field()
	password: string
}