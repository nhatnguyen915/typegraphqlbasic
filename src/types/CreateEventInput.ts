import { Field, InputType } from "type-graphql";
import { Event } from "../models/Event";


@InputType()
export class CreateEventInput implements Partial<Event> {
    @Field()
    public name!: string;

    @Field()
    public code!: string;

    @Field()
    public quantity?: number;
}