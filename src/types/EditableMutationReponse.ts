import { Field, ObjectType } from 'type-graphql'
import { User } from '../models/User'
import { IMutationResponse } from './MutationResponse'

@ObjectType({ implements: IMutationResponse })
export class EditableMutationResponse implements IMutationResponse {
    code: number
    success: boolean
    message?: string
}