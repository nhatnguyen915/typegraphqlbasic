import { Field, InputType } from "type-graphql";
import { Voucher } from "../models/Voucher";

@InputType()
export class CreateVoucherInput implements Partial<Voucher> {
    @Field()
    public name!: string;

    @Field()
    public code!: string;

    @Field()
    public eventId!: number;
}