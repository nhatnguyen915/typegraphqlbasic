import { buildSchema } from "type-graphql";

import { CreateEvent } from "./modules/event/CreateEvent";
import { EditableMaintain } from "./modules/Event/EditableMaintain";
import { EditableMe } from "./modules/Event/EditableMe";
import { EditableRelease } from "./modules/Event/EditableRelease";
import { GetEvent } from "./modules/event/GetEvent";
import { Login, Register } from "./modules/User/User";
import { CreateVoucher } from "./modules/Voucher.ts/IssuedVoucher";

export default (Container: any) => {
    return buildSchema({
        container: Container,
        resolvers: [CreateEvent, GetEvent, CreateVoucher, Register, Login, EditableMe, EditableRelease, EditableMaintain],
    });
};