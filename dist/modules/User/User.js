"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Login = exports.Register = void 0;
const type_graphql_1 = require("type-graphql");
const typeorm_1 = require("typeorm");
const UserRepository_1 = require("../../repositories/UserRepository");
const UserMutaionReponse_1 = require("../../types/UserMutaionReponse");
const bcrypt_1 = __importDefault(require("bcrypt"));
const RegisterInput_1 = require("../../types/RegisterInput");
const User_1 = require("../../models/User");
const LoginInput_1 = require("../../types/LoginInput");
const jwtService_1 = require("../../utils/jwtService");
let Register = class Register {
    registerUser(inputData) {
        return __awaiter(this, void 0, void 0, function* () {
            const userRepository = (0, typeorm_1.getCustomRepository)(UserRepository_1.UserRepository);
            const existingUser = yield userRepository.findOne(inputData.username);
            if (existingUser) {
                return {
                    code: 400,
                    success: false,
                    message: 'Duplicated username'
                };
            }
            const salt = yield bcrypt_1.default.genSalt(10);
            const hashPassword = yield bcrypt_1.default.hash(inputData.password, salt);
            const category = userRepository.create({
                username: inputData.username,
                password: hashPassword,
            });
            const newUser = yield userRepository.save(category);
            return {
                code: 200,
                success: true,
                message: 'User registration successful',
                user: newUser
            };
        });
    }
};
__decorate([
    (0, type_graphql_1.Mutation)(_return => UserMutaionReponse_1.UserMutationResponse),
    __param(0, (0, type_graphql_1.Arg)("data")),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [RegisterInput_1.RegisterInput]),
    __metadata("design:returntype", Promise)
], Register.prototype, "registerUser", null);
Register = __decorate([
    (0, type_graphql_1.Resolver)((_type) => User_1.User)
], Register);
exports.Register = Register;
class Login {
    login(inputData, { res }) {
        return __awaiter(this, void 0, void 0, function* () {
            const existingUser = yield User_1.User.findOne({ username: inputData.username });
            if (!existingUser) {
                return {
                    code: 400,
                    success: false,
                    message: 'User not found'
                };
            }
            console.log(existingUser.password);
            console.log(inputData.password);
            const isPasswordValid = yield bcrypt_1.default.compare(inputData.password, existingUser.password);
            console.log(isPasswordValid);
            if (!isPasswordValid) {
                return {
                    code: 400,
                    success: false,
                    message: 'Incorrect password'
                };
            }
            (0, jwtService_1.sendRefreshToken)(res, existingUser);
            return {
                code: 200,
                success: true,
                message: 'Logged in successfully',
                user: existingUser,
                accessToken: (0, jwtService_1.createToken)('accessToken', existingUser)
            };
        });
    }
}
__decorate([
    (0, type_graphql_1.Mutation)(_return => UserMutaionReponse_1.UserMutationResponse),
    __param(0, (0, type_graphql_1.Arg)('data')),
    __param(1, (0, type_graphql_1.Ctx)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [LoginInput_1.LoginInput, Object]),
    __metadata("design:returntype", Promise)
], Login.prototype, "login", null);
exports.Login = Login;
//# sourceMappingURL=User.js.map