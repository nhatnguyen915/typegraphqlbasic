"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.EditableMantain = void 0;
const type_graphql_1 = require("type-graphql");
const typeorm_1 = require("typeorm");
const authUser_1 = require("../../middleware/authUser");
const EventEditLog_1 = require("../../models/EventEditLog");
const EventRepository_1 = require("../../repositories/EventRepository");
const EditableMutationReponse_1 = require("../../types/EditableMutationReponse");
const redisService_1 = require("../../utils/redisService");
let EditableMantain = class EditableMantain {
    editableMatain(eventId, { user }) {
        return __awaiter(this, void 0, void 0, function* () {
            const eventResponse = (0, typeorm_1.getCustomRepository)(EventRepository_1.EventRepository);
            const event = yield eventResponse.findOne(eventId);
            if (!event) {
                return {
                    code: 400,
                    success: false,
                    message: "Event not found"
                };
            }
            const keyStr = eventId.toString() + '-editing';
            const keyName = yield (0, redisService_1.exists)(keyStr);
            if (!keyName) {
                return {
                    code: 400,
                    success: true,
                    message: 'Can not relase event is not editing  ',
                };
            }
            if (event.userEditing != user.userId) {
                return {
                    code: 200,
                    success: true,
                    message: 'This user can not relase event ',
                };
            }
            (0, redisService_1.expire)(keyStr, 300); // set expire 300s
            return {
                code: 200,
                success: true,
                message: 'Successfully ',
            };
        });
    }
};
__decorate([
    (0, type_graphql_1.Mutation)(_return => EditableMutationReponse_1.EditableMutationResponse),
    (0, type_graphql_1.UseMiddleware)(authUser_1.authUser),
    __param(0, (0, type_graphql_1.Arg)("eventId", () => type_graphql_1.Int)),
    __param(1, (0, type_graphql_1.Ctx)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, Object]),
    __metadata("design:returntype", Promise)
], EditableMantain.prototype, "editableMatain", null);
EditableMantain = __decorate([
    (0, type_graphql_1.Resolver)((_type) => EventEditLog_1.EventEditLog)
], EditableMantain);
exports.EditableMantain = EditableMantain;
//# sourceMappingURL=EditableMatain.js.map