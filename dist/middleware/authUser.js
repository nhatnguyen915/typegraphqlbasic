"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.authUser = void 0;
const apollo_server_express_1 = require("apollo-server-express");
const jsonwebtoken_1 = require("jsonwebtoken");
const token = "BbZJjyoXAdr8BUZuiKKARWimKfrSmQ6fv8kZ7OFfc";
const cookie = "jwt-auth-cookie-name";
const authUser = ({ context }, next) => {
    try {
        // authHeader here is "Bearer accessToken"
        const authHeader = context.req.header('Authorization');
        const accessToken = authHeader && authHeader.split(' ')[1];
        if (!accessToken)
            throw new apollo_server_express_1.AuthenticationError('Not authenticated to perform GraphQL operations');
        const decodedUser = (0, jsonwebtoken_1.verify)(accessToken, token);
        context.user = decodedUser;
        return next();
    }
    catch (error) {
        throw new apollo_server_express_1.AuthenticationError(`Error authenticating user, ${JSON.stringify(error)}`);
    }
};
exports.authUser = authUser;
//# sourceMappingURL=authUser.js.map