"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Event = void 0;
const typeorm_1 = require("typeorm");
const type_graphql_1 = require("type-graphql");
const Voucher_1 = require("./Voucher");
const EventEditLog_1 = require("./EventEditLog");
let Event = class Event {
    constructor() {
        this.userEditing = 0;
    }
};
__decorate([
    (0, type_graphql_1.Field)((_type) => Number),
    (0, typeorm_1.PrimaryGeneratedColumn)(),
    __metadata("design:type", Number)
], Event.prototype, "id", void 0);
__decorate([
    (0, type_graphql_1.Field)(),
    (0, typeorm_1.Column)({ type: "varchar" }),
    __metadata("design:type", String)
], Event.prototype, "code", void 0);
__decorate([
    (0, type_graphql_1.Field)(),
    (0, typeorm_1.Column)({ type: "varchar" }),
    __metadata("design:type", String)
], Event.prototype, "name", void 0);
__decorate([
    (0, type_graphql_1.Field)((_type) => Number),
    (0, typeorm_1.Column)(),
    __metadata("design:type", Number)
], Event.prototype, "quantity", void 0);
__decorate([
    (0, type_graphql_1.Field)((_type) => Number),
    (0, typeorm_1.Column)(),
    __metadata("design:type", Number)
], Event.prototype, "userEditing", void 0);
__decorate([
    (0, type_graphql_1.Field)(),
    (0, typeorm_1.CreateDateColumn)(),
    __metadata("design:type", Date)
], Event.prototype, "createdAt", void 0);
__decorate([
    (0, type_graphql_1.Field)(),
    (0, typeorm_1.UpdateDateColumn)(),
    __metadata("design:type", Date)
], Event.prototype, "updatedAt", void 0);
__decorate([
    (0, type_graphql_1.Field)((_type) => [Voucher_1.Voucher]),
    (0, typeorm_1.OneToMany)((_type) => Voucher_1.Voucher, (voucher) => voucher.event),
    __metadata("design:type", Array)
], Event.prototype, "voucher", void 0);
__decorate([
    (0, type_graphql_1.Field)((_type) => [EventEditLog_1.EventEditLog]),
    (0, typeorm_1.OneToMany)((_type) => EventEditLog_1.EventEditLog, (eventEditLog) => eventEditLog.event),
    __metadata("design:type", Array)
], Event.prototype, "eventEditLog", void 0);
Event = __decorate([
    (0, type_graphql_1.ObjectType)(),
    (0, typeorm_1.Entity)(),
    (0, typeorm_1.Unique)(["code"])
], Event);
exports.Event = Event;
//# sourceMappingURL=Event.js.map