"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const type_graphql_1 = require("type-graphql");
const CreateEvent_1 = require("./modules/event/CreateEvent");
const EditableMaintain_1 = require("./modules/Event/EditableMaintain");
const EditableMe_1 = require("./modules/Event/EditableMe");
const EditableRelease_1 = require("./modules/Event/EditableRelease");
const GetEvent_1 = require("./modules/event/GetEvent");
const User_1 = require("./modules/User/User");
const IssuedVoucher_1 = require("./modules/Voucher.ts/IssuedVoucher");
exports.default = (Container) => {
    return (0, type_graphql_1.buildSchema)({
        container: Container,
        resolvers: [CreateEvent_1.CreateEvent, GetEvent_1.GetEvent, IssuedVoucher_1.CreateVoucher, User_1.Register, User_1.Login, EditableMe_1.EditableMe, EditableRelease_1.EditableRelease, EditableMaintain_1.EditableMaintain],
    });
};
//# sourceMappingURL=schema.js.map