"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.sendRefreshToken = exports.createToken = void 0;
const jsonwebtoken_1 = require("jsonwebtoken");
const token = "BbZJjyoXAdr8BUZuiKKARWimKfrSmQ6fv8kZ7OFfc";
const cookie = "jwt-auth-cookie-name";
const createToken = (type, user) => (0, jsonwebtoken_1.sign)(Object.assign({ userId: user.id }, (type === 'refreshToken' ? { tokenVersion: user.tokenVersion } : {})), type === 'accessToken'
    ? token
    : token, {
    expiresIn: type === 'accessToken' ? '60m' : '60m'
});
exports.createToken = createToken;
const sendRefreshToken = (res, user) => {
    res.cookie(cookie, (0, exports.createToken)('refreshToken', user), {
        httpOnly: true,
        secure: true,
        sameSite: 'lax',
        path: '/refresh_token'
    });
};
exports.sendRefreshToken = sendRefreshToken;
//# sourceMappingURL=jwtService.js.map