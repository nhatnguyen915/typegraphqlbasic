"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.buildEventkey = exports.del = exports.ttl = exports.incrby = exports.expire = exports.setnx = exports.set = exports.exists = exports.get = void 0;
const redis = require("redis");
const client = redis.createClient();
(() => __awaiter(void 0, void 0, void 0, function* () {
    yield client.connect();
}))();
client.on('connect', () => console.log('::> Redis Client Connected'));
client.on('error', (err) => console.log('<:: Redis Client Error', err));
const get = (key) => __awaiter(void 0, void 0, void 0, function* () {
    return new Promise((resolve, reject) => {
        try {
            const result = client.get(key);
            resolve(result);
        }
        catch (error) {
            reject(error);
        }
    });
});
exports.get = get;
const exists = (key) => __awaiter(void 0, void 0, void 0, function* () {
    return new Promise((resolve, reject) => {
        try {
            const result = client.exists(key);
            resolve(result);
        }
        catch (error) {
            reject(error);
        }
    });
});
exports.exists = exists;
const set = (key, value, duration) => __awaiter(void 0, void 0, void 0, function* () {
    return new Promise((resolve, reject) => {
        try {
            client.set(key, value);
            client.expire(key, duration);
            resolve(true);
        }
        catch (error) {
            return reject(error);
        }
    });
});
exports.set = set;
const setnx = (key, value, duration) => __awaiter(void 0, void 0, void 0, function* () {
    return new Promise((resolve, reject) => {
        try {
            client.setNX(key, value);
            client.expire(key, duration);
            resolve(true);
        }
        catch (error) {
            return reject(error);
        }
    });
});
exports.setnx = setnx;
const expire = (key, duration) => __awaiter(void 0, void 0, void 0, function* () {
    return new Promise((resolve, reject) => {
        try {
            client.expire(key, duration);
            resolve(true);
        }
        catch (error) {
            return reject(error);
        }
    });
});
exports.expire = expire;
const incrby = (key, value) => __awaiter(void 0, void 0, void 0, function* () {
    return new Promise((resolve, reject) => {
        try {
            const result = client.incr(key, value);
            resolve(result);
        }
        catch (error) {
            return reject(error);
        }
    });
});
exports.incrby = incrby;
const ttl = (key) => __awaiter(void 0, void 0, void 0, function* () {
    return new Promise((resolve, reject) => {
        try {
            const result = client.ttl(key);
            resolve(result);
        }
        catch (error) {
            return reject(error);
        }
    });
});
exports.ttl = ttl;
const del = (key) => __awaiter(void 0, void 0, void 0, function* () {
    return new Promise((resolve, reject) => {
        try {
            client.del(key);
            resolve(true);
        }
        catch (error) {
            return reject(error);
        }
    });
});
exports.del = del;
const buildEventkey = (eventId) => {
    return eventId.toString() + '-editing';
};
exports.buildEventkey = buildEventkey;
//# sourceMappingURL=redisService.js.map